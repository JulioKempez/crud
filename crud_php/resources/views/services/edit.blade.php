@extends('layouts.app')

@section('content')

<div class="container">

<form action="{{ url('/services/'. $service->id) }}" method="post">
    
    {{ csrf_field() }}
    {{ method_field('PATCH') }}

    @include('services.form',['Modo'=>'edit'])

</form>

</div>
@endsection