@extends('layouts.app')

@section('content')

<div class="container">

<h1>Inicio</h1>

@if (Session::has('Message'))

    <div class="alert alert-success" role="alert">
        {{Session::get('Message')}}
    </div>

@endif
<br>

<a href="{{ url('services/create') }}" class="btn btn-outline-success">Add service</a>
<table class="table table-hover">
    <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Country</th>
            <th>City</th>
            <th>Pick up Time</th>
            <th>Party</th>
            <th>Airline</th>
            <th>Flight</th>
            <th>Date</th>
            <th>Destination</th>
            <th></th>
            <th></th>
            
        </tr>
    </thead>


    <tbody>
        @foreach($services as $service)
        <tr>
        <td>{{$loop-> iteration}}</td>
        <td>{{ $service->firstname}}</td>
        <td>{{ $service->lastname}}</td>
        <td>{{ $service->email}}</td>
        <td>{{ $service->phone}}</td>
        <td>{{ $service->country}}</td>
        <td>{{ $service->city}}</td>
        <td>{{ $service->pickup}}</td>
        <td>{{ $service->nparty}}</td>
        <td>{{ $service->airline}}</td>
        <td>{{ $service->flightnum}}</td>
        <td>{{ $service->date}}</td>
        <td>{{ $service->destination}}</td>
        <td>
            
            <a href="{{ url('/services/'.$service->id.'/edit')}}" class="btn btn-outline-primary">
            Edit
            </a>
        </td>
        <td>
        <form action="{{ url('/services/'.$service->id) }}" method="post">
        {{ csrf_field() }}
        {{method_field('DELETE')}}
        <button type="submit" onclick="return confirm('Do you want to Delete?');" class="btn btn-outline-danger">Delete</button>
        </td>
        </form>

        </tr>
        @endforeach
    </tbody>
</table>

    {{ $services->links()}}

</div>
@endsection