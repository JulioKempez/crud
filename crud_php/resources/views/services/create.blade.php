@extends('layouts.app')

@section('content')

<div class="container">


<form action="{{ url('/services')}}" class="form-group" method="post">

{{ csrf_field() }}
@include('services.form',['Modo'=>'crear'])


</form>

</div>
@endsection