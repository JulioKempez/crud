#READ ME#
El problema planteado es: 
Existe una empresa que necesita un panel de control para administrar los viajes agendados a través de su página web.
Realizar dicho panel con las siguientes características.

° Se deberá poder visualizar los datos almacenados en la base de datos
° En el panel se deberán encontrar dos botones para editar o eliminar los registros de la base de datos
° Se deberá poder agregar nuevos datos a la tabla por medio de una vista separada
° La base de datos no podrá contener campos vacíos
° No se deben guardar en la tabla los datos que no correspondan a el campo requerido
° La tabla deberá contener los siguientes campos: Nombre, Apellido, Dirección de Correo, Numero de Telefono, Pais, Ciudad, Hora de llegada, Número de Pasajeros, Aerolínea en la que viaja, Número de vuelo, Fecha y Destino.
° No se podrá acceder al panel de control y por consiguiente no se podrá modificar, eliminar, ni agregar datos a la base de datos a menos que el usuario tenga permisos de Administrador.

#user=  admin@hotmail.com#
#password= administrador#