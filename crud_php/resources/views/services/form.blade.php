
<h1> {{ $Modo=='crear' ? 'Create Service': 'Edit Service' }} </h1>
<br>

<form>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="firstname">{{'First Name'}}</label>
        <input type="text" class="form-control {{$errors->has('firstname')?'is-invalid':''}}"name="firstname" id="firstname" 
                    value="{{ isset($service->firstname)?$service->firstname:old('firstname') }}"
        >
        {!! $errors->first('firstname','<div class="invalid-feedback">:message</div>') !!}
    </div>
    <div class="form-group col-md-6"> 
        <label for="lastname" class="control-label">{{'Last Name'}}</label>
        <input type="text" class="form-control {{$errors->has('lastname')?'is-invalid':''}}" name="lastname" id="lastname" 
                value="{{ isset($service->lastname)?$service->lastname:old('lastname') }}"
        >
        {!! $errors->first('lastname','<div class="invalid-feedback">:message</div>') !!}
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-8">
        <label for="email">{{'Email'}}</label>
        <input type="email" class="form-control {{$errors->has('email')?'is-invalid':''}}" name="email" id="email"
            value="{{ isset($service->email)?$service->email:old('email') }}"
        >
        {!! $errors->first('email','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group col-md-4">
        <label for="phone">{{'Phone'}}</label>
        <input type="number" class="form-control {{$errors->has('phone')?'is-invalid':''}}" name="phone" id="phone" 
        value="{{ isset($service->phone)?$service->phone:old('phone') }}"
        >
        {!! $errors->first('phone','<div class="invalid-feedback">:message</div>') !!}
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label for="country">{{'Country'}}</label>
        <input type="text" class="form-control {{$errors->has('country')?'is-invalid':''}}" name="country" id="country"
        value="{{ isset($service->country)?$service->country:old('country') }}"
        >
        {!! $errors->first('country','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group col-md-6">
        <label for="city">{{'City'}}</label>
        <input type="text" class="form-control {{$errors->has('city')?'is-invalid':''}}" name="city" id="city" 
            value="{{ isset($service->city)?$service->city:old('city') }}"
        >
        {!! $errors->first('city','<div class="invalid-feedback">:message</div>') !!}
    </div>
</div>

<div class="form-row">

    <div class="form-group col-md-4">
        <label for="flightnum" class="control-label">{{'Flight Number'}}</label>
        <input type="number" class="form-control {{$errors->has('flightnum')?'is-invalid':''}}" name="flightnum" id="flightnum"
            value="{{ isset($service->flightnum)?$service->flightnum:old('flightnum') }}"
            >
            {!! $errors->first('flightnum','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group col-md-4">
        <label for="pickup">{{'Pick Up Time'}}</label>
        <input type="time" class="form-control {{$errors->has('pickup')?'is-invalid':''}}" name="pickup" id="pickup" 
            value="{{ isset($service->pickup)?$service->pickup:old('pickup') }}"
            >
            {!! $errors->first('pickup','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group col-md-4">
        <label for="date">{{'Date'}}</label>
        <input type="date" class="form-control {{$errors->has('date')?'is-invalid':''}}" name="date" id="date" 
        value="{{ isset($service->date)?$service->date:old('date') }}"
        >
        {!! $errors->first('date','<div class="invalid-feedback">:message</div>') !!}
        </div>
</div>

<div class="form-row">

    <div class="form-group col-md-4">
        <label for="airline" class="control-label">{{'Airline'}}</label>
        <input type="text" class="form-control {{$errors->has('airline')?'is-invalid':''}}" name="airline" id="airline" 
        value="{{ isset($service->airline)?$service->airline:old('airline') }}"
        >
        {!! $errors->first('airline','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group col-md-4">
        <label for="nparty">{{'Party Members'}}</label>
        <input type="number" class="form-control {{$errors->has('nparty')?'is-invalid':''}}" name="nparty" id="nparty" 
        value="{{ isset($service->nparty)?$service->nparty:old('nparty') }}"
        >
        {!! $errors->first('nparty','<div class="invalid-feedback">:message</div>') !!}
    </div>

    <div class="form-group col-md-4">
        <label for="destination" class="control-label">{{'Destination'}}</label>
        <input type="text" class="form-control {{$errors->has('destination')?'is-invalid':''}}" name="destination" id="destination" 
        value="{{ isset($service->destination)?$service->destination:old('destination') }}"
        >
        {!! $errors->first('destination','<div class="invalid-feedback">:message</div>') !!}
    </div>
</div>


<input type="submit" class="btn btn-outline-primary" value="{{ $Modo=='crear' ? 'Create': 'Edit' }}">

<a href="{{ url('services') }}" class="btn btn-outline-success">Return</a>
</form>