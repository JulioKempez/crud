<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['services']=Service::paginate(3);
        return view('services.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $campos=[
            'firstname' => 'required|string|max:100',
            'lastname' => 'required|string|max:100',
            'email' => 'required|email',
            'phone' => 'required',
            'country' => 'required|string|max:50',
            'city' => 'required|string|max:50',
            'flightnum' => 'required',
            'pickup' => 'required',
            'date' => 'required',
            'airline' => 'required|string|max:30',
            'nparty' => 'required',
            'destination' => 'required|string|max:30'
        ];

        $Message=["required"=>'The :attribute is required'];
        $this->validate($request,$campos,$Message);


        $datosService=request()->except('_token');

        Service::insert($datosService);

        return redirect('services')->with('Message','Created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $service= Service::findOrFail($id);

         return view('services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //

        $campos=[
            'firstname' => 'required|string|max:100',
            'lastname' => 'required|string|max:100',
            'email' => 'required|email',
            'phone' => 'required',
            'country' => 'required|string|max:50',
            'city' => 'required|string|max:50',
            'flightnum' => 'required',
            'pickup' => 'required',
            'date' => 'required',
            'airline' => 'required|string|max:30',
            'nparty' => 'required',
            'destination' => 'required|string|max:30'
        ];

        $Message=["required"=>'The :attribute is required'];
        $this->validate($request,$campos,$Message);

        $datosService=request()->except(['_token','_method']);
        Service::where('id','=',$id)->update($datosService);

        $service= Service::findOrFail($id);

        return redirect('services')->with('Message','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Service::destroy($id);
        return redirect('services')->with('Message','Successfully Deleted');
    }
}
